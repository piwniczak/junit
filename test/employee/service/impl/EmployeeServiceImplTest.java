package employee.service.impl;
import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.is; 
import static org.hamcrest.Matchers.*; 

import contract.service.ContractServiceExt;
import employee.model.Employee;
import employee.model.EmployeeBuilder;
import employee.service.NoEmployeeFoundBusinessException;
import employee.store.EmployeeRepository;
import payment.calculator.PaymentCalculator;


public class EmployeeServiceImplTest {

    private static final int FEBRUARY = 1;
    private static final int SALARY = 1000;
    private static EmployeeServiceImpl employeeServiceTested;
    private static ContractServiceExt contractServiceExtMocked;
    private static EmployeeRepository employeeRepositoryMocked;
    private static PaymentCalculator  paymentCalculatorMocked;
    
    @Before
    public void setUp() throws Exception {
        employeeServiceTested = new EmployeeServiceImpl(); 
        contractServiceExtMocked = Mockito.mock(ContractServiceExt.class);
        employeeServiceTested.setContractServiceExt(contractServiceExtMocked);
        paymentCalculatorMocked = Mockito.mock(PaymentCalculator.class);
        employeeServiceTested.setPaymentCalculator(paymentCalculatorMocked);
        employeeRepositoryMocked = Mockito.mock(EmployeeRepository.class);
        employeeServiceTested.setEmployeeRepository(employeeRepositoryMocked);

    }
    @Test
    public void shouldReturnSalaryForTheGivenEmployeAndMonth() {
        //given
        Employee employee = EmployeeBuilder
                .anEmployee()
                .correctWorkingEmployee()
                .build();
        
        Mockito.when(employeeRepositoryMocked.findAll()).thenReturn(Arrays.asList(employee));
        Mockito.when(contractServiceExtMocked.getSalaryFromContract(employee.getId(), FEBRUARY)).thenReturn(BigDecimal.valueOf(SALARY));
        Mockito.when(paymentCalculatorMocked.getPaymentAmountForMonth(employee, BigDecimal.valueOf(SALARY), FEBRUARY))
            .thenReturn(BigDecimal.valueOf(SALARY));
        //when
        BigDecimal payment = employeeServiceTested.getPaymentAmountForMonth(employee.getName(), employee.getSurname(), FEBRUARY);
        //then
        assertThat(payment, is(equalTo(new BigDecimal(SALARY))));
        Mockito.verify(contractServiceExtMocked).getSalaryFromContract(employee.getId(), FEBRUARY);
        Mockito.verify(paymentCalculatorMocked).getPaymentAmountForMonth(employee, BigDecimal.valueOf(SALARY), FEBRUARY);
        Mockito.verify(paymentCalculatorMocked).getPaymentAmountForMonth(employee, BigDecimal.valueOf(SALARY), FEBRUARY);
        
    }
    
    @Test(expected = NoEmployeeFoundBusinessException.class)
    public void shouldThrowNoEmployeeFoundBusinessExceptionWhenEmplWhenEmployeeNameWasNotFOundInRepository()  { 
        //given
        Employee employee = EmployeeBuilder
                .anEmployee()
                .correctWorkingEmployee()
                .build();
        
        Employee employeeWrong = EmployeeBuilder
                .anEmployee()
                .correctWorkingEmployee()
                .but()
                .withName("Jan")
                .build();
        
        Mockito.when(employeeRepositoryMocked.findAll()).thenReturn(Arrays.asList(employeeWrong));
        //when
        BigDecimal result = employeeServiceTested.getPaymentAmountForMonth(employee.getName(), employee.getSurname(), FEBRUARY);
        //then
        assertThat(result, is( new NoEmployeeFoundBusinessException(employee.getName(), employee.getName())));
        Mockito.verify(employeeRepositoryMocked).findAll();
    }
    @Test(expected = NoEmployeeFoundBusinessException.class)
    public void shouldThrowNoEmployeeFoundBusinessExceptionWhenEmplWhenEmployeeSurnameWasNotFOundInRepository()  { 
        //given
        Employee employee = EmployeeBuilder
                .anEmployee()
                .correctWorkingEmployee()
                .build();
        
        Employee employeeWrong = EmployeeBuilder
                .anEmployee()
                .correctWorkingEmployee()
                .but()
                .withSurname("Walec")
                .build();
        
        Mockito.when(employeeRepositoryMocked.findAll()).thenReturn(Arrays.asList(employeeWrong));
        //when
        BigDecimal result = employeeServiceTested.getPaymentAmountForMonth(employee.getName(), employee.getSurname(), FEBRUARY);
        //then
        assertThat(result, is( new NoEmployeeFoundBusinessException(employee.getName(), employee.getName())));
        Mockito.verify(employeeRepositoryMocked).findAll();
    }
    @Test(expected = NoEmployeeFoundBusinessException.class)
    public void shouldThrowNoEmployeeFoundBusinessExceptionWhenEmplWhenEmployeesurNameAndNameWasNotFoundInRepository()  { 
        //given
        Employee employee = EmployeeBuilder
                .anEmployee()
                .correctWorkingEmployee()
                .build();
        
        Employee employeeWrong = EmployeeBuilder
                .anEmployee()
                .correctWorkingEmployee()
                .but()
                .withName("Jan")
                .withSurname("Walec")
                .build();
        
        Mockito.when(employeeRepositoryMocked.findAll()).thenReturn(Arrays.asList(employeeWrong));
        //when
        BigDecimal payment = employeeServiceTested.getPaymentAmountForMonth(employee.getName(), employee.getSurname(), FEBRUARY);
        //then
        assertThat(payment, is( new NoEmployeeFoundBusinessException(employee.getName(), employee.getName())));
        Mockito.verify(employeeRepositoryMocked).findAll();
    }
    

}
