package employee.model;

import java.time.LocalDate;

public class EmployeeBuilder {
private Employee underConstruction = null;
    
    public static EmployeeBuilder anEmployee() {
        EmployeeBuilder builder = new EmployeeBuilder();
        builder.underConstruction = new Employee();
        return builder;
    }
    
    public EmployeeBuilder correctWorkingEmployee() {
        withName("John");
        withSurname("Brown");
        withEmploymentStatus(EmploymentStatus.EMPLOYED);
        withId(123456789L);
        withDateOfEmployment(LocalDate.of(2014, 12, 8));
        withDateOfLeaving(LocalDate.of(2020, 12, 26));
        return this;
    }
    
    public EmployeeBuilder but() {
        return this;
    }    
    
    public EmployeeBuilder withName(String name) {
        underConstruction.name = name;
        return this;
    }
    
    public EmployeeBuilder withSurname(String surname) {
        underConstruction.surname = surname;
        return this;
    }
    
    public EmployeeBuilder withId(Long id) {
        underConstruction.id = id;
        return this;
    }
    public EmployeeBuilder withDateOfEmployment(LocalDate dateOfEmployment) {
        underConstruction.dateOfEmployment = dateOfEmployment;
        return this;
    }
    
    public EmployeeBuilder withDateOfLeaving(LocalDate dateOfLeaving) {
        underConstruction.dateOfLeaving = dateOfLeaving;
        return this;
    }
    
    public EmployeeBuilder withEmploymentStatus(EmploymentStatus employmentStatus) {
        underConstruction.employmentStatus = employmentStatus;
        return this;
    }
    
    public Employee build() {
        Employee builded = underConstruction;
        underConstruction = new Employee();
        return builded;
    }
}
