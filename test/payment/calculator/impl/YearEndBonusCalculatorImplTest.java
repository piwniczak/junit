package payment.calculator.impl;
import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.is; 
import static org.hamcrest.Matchers.*; 


import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import employee.model.Employee;
import employee.model.EmployeeBuilder;
import employee.model.EmploymentStatus;

public class YearEndBonusCalculatorImplTest {
     static YearEndBonusCalculatorImpl yearEndBonusCalculator;
     private final BigDecimal  SALARY = new BigDecimal(1000);
     private final BigDecimal  BONUS20 = SALARY.multiply(new BigDecimal(0.2));
     private final BigDecimal  BONUS30 = SALARY.multiply(new BigDecimal(0.3));
     private final BigDecimal  BONUS50 = SALARY.multiply(new BigDecimal(0.5));
     private EmployeeBuilder employeeInBuild;
    
    @Before
    public void setUp() throws Exception {
    }
    
    @Before public  void beforeEach() {
        employeeInBuild = EmployeeBuilder
                .anEmployee()
                .correctWorkingEmployee();
    }
    
    @Test
    public void shouldYearEndBonusEqualsZeroWhenEmployeeIsRetired() {
        
        //given
        BigDecimal expectedBonus = new BigDecimal(0);
        Employee newEmployee = employeeInBuild
                                .but()
                                .withEmploymentStatus(EmploymentStatus.RETIRED)
                                .build();                       

        YearEndBonusCalculatorImpl yearEndBonusCalculator = new YearEndBonusCalculatorImpl();
        //when
        BigDecimal bonus = yearEndBonusCalculator.getYearEndBonus(newEmployee, SALARY);
        //then
        assertThat(bonus, is(equalTo(expectedBonus)));

    }
    @Test
    public void shouldYearEndBonusEqualsZeroWhenEmployeeIsLeaver() {
        
        //given
        BigDecimal expectedBonus = new BigDecimal(0);
        Employee newEmployee = employeeInBuild
                                .but()
                                .withEmploymentStatus(EmploymentStatus.LEAVER)
                                .build();                       

        YearEndBonusCalculatorImpl yearEndBonusCalculator = new YearEndBonusCalculatorImpl();
        //when
        BigDecimal bonus = yearEndBonusCalculator.getYearEndBonus(newEmployee, SALARY);
        //then
        assertThat(bonus, is(equalTo(expectedBonus)));
    }
    
    
    @Test
    public void shouldYearEndBonusEqualsZeroWhenEmployeeIsSelfEmploeyd() {
        
        //given
        BigDecimal expectedBonus = new BigDecimal(0);
        Employee newEmployee = employeeInBuild
                                .but()
                                .withEmploymentStatus(EmploymentStatus.SELF_EMPLOYED)
                                .build();                       

        YearEndBonusCalculatorImpl yearEndBonusCalculator = new YearEndBonusCalculatorImpl();
        //when
        BigDecimal bonus = yearEndBonusCalculator.getYearEndBonus(newEmployee, SALARY);
        //then
        assertThat(bonus, is(equalTo(expectedBonus)));
    }
    
    
    @Test
    public void shouldNotYearEndBonus0When3Years() {
        
        //given
        BigDecimal expectedBonus = new BigDecimal(0);
        Employee newEmployee = employeeInBuild
                                .but().withEmploymentStatus(EmploymentStatus.EMPLOYED)
                                .but().withDateOfEmployment(LocalDate.of(2015, 12, 8))
                                .build();                       

        YearEndBonusCalculatorImpl yearEndBonusCalculator = new YearEndBonusCalculatorImpl();
        //when
        BigDecimal bonus = yearEndBonusCalculator.getYearEndBonus(newEmployee, SALARY);
        //then
        assertThat(bonus, is(equalTo(expectedBonus)));
    }
    @Test
    public void shouldYearEndBonus20When4Years() {
        
        //given
        BigDecimal expectedBonus = BONUS20;
        Employee newEmployee = employeeInBuild

                                .but().withEmploymentStatus(EmploymentStatus.EMPLOYED)
                                .but().withDateOfEmployment(LocalDate.of(2014, 12, 8))
                                .build();                       

        YearEndBonusCalculatorImpl yearEndBonusCalculator = new YearEndBonusCalculatorImpl();
        //when
        BigDecimal bonus = yearEndBonusCalculator.getYearEndBonus(newEmployee, SALARY);
        //then
        assertThat(bonus, is(equalTo(expectedBonus)));
    }
    @Test
    public void shouldYearEndBonus20When5Years() {
        
        //given
        BigDecimal expectedBonus = BONUS20;
        Employee newEmployee = employeeInBuild

                                .but().withEmploymentStatus(EmploymentStatus.EMPLOYED)
                                .but().withDateOfEmployment(LocalDate.of(2013, 12, 8))
                                .build();                       

        YearEndBonusCalculatorImpl yearEndBonusCalculator = new YearEndBonusCalculatorImpl();
        //when
        BigDecimal bonus = yearEndBonusCalculator.getYearEndBonus(newEmployee, SALARY);
        //then
        assertThat(bonus, is(equalTo(expectedBonus)));
    }
    @Test
    public void shouldYearEndBonus30When6Years() {
        
        //given
        BigDecimal expectedBonus = BONUS30;
        Employee newEmployee = employeeInBuild

                                .but().withEmploymentStatus(EmploymentStatus.EMPLOYED)
                                .but().withDateOfEmployment(LocalDate.of(2012, 12, 8))
                                .build();                       

        YearEndBonusCalculatorImpl yearEndBonusCalculator = new YearEndBonusCalculatorImpl();
        //when
        BigDecimal bonus = yearEndBonusCalculator.getYearEndBonus(newEmployee, SALARY);
        //then
        assertThat(bonus, is(equalTo(expectedBonus)));
    }
    @Test
    public void shouldYearEndBonus30When8Years() {
        
        //given
        BigDecimal expectedBonus = BONUS30;
        Employee newEmployee = employeeInBuild

                                .but().withEmploymentStatus(EmploymentStatus.EMPLOYED)
                                .but().withDateOfEmployment(LocalDate.of(2010, 12, 8))
                                .build();                       

        YearEndBonusCalculatorImpl yearEndBonusCalculator = new YearEndBonusCalculatorImpl();
        //when
        BigDecimal bonus = yearEndBonusCalculator.getYearEndBonus(newEmployee, SALARY);
        //then
        assertThat(bonus, is(equalTo(expectedBonus)));
    }
    @Test
    public void shouldYearEndBonus50When9Years() {
        
        //given
        BigDecimal expectedBonus = BONUS50;
        Employee newEmployee = employeeInBuild

                                .but().withEmploymentStatus(EmploymentStatus.EMPLOYED)
                                .but().withDateOfEmployment(LocalDate.of(2009, 12, 8))
                                .build();                       

        YearEndBonusCalculatorImpl yearEndBonusCalculator = new YearEndBonusCalculatorImpl();
        //when
        BigDecimal bonus = yearEndBonusCalculator.getYearEndBonus(newEmployee, SALARY);
        //then
        assertThat(bonus, is(equalTo(expectedBonus)));
    }

    
}