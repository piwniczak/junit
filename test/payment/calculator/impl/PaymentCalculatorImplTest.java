package payment.calculator.impl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;


import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import employee.model.Employee;
import employee.model.EmployeeBuilder;
import org.mockito.Mockito;

public class PaymentCalculatorImplTest {
    private static final int ZERO_BONUS = 0;
    private static final int JANUARY = 0;
    private static final int DECEMBER = 11;
    private static final int SALARY = 3000;
    private static final int BONUS = 200;
    private static final int SALARY_WITH_BONUS = 3200;
    private static PaymentCalculatorImpl  paymentCalculatorImpl;
    private static YearEndBonusCalculatorImpl yearEndBonusCalculatorImplMocked;
    
    @Before
    public void setUp() {
        paymentCalculatorImpl = new PaymentCalculatorImpl();
        yearEndBonusCalculatorImplMocked = Mockito.mock(YearEndBonusCalculatorImpl.class);
        paymentCalculatorImpl.setYearEndBonusCalculatorImpl(yearEndBonusCalculatorImplMocked);
    }
    
    @Test
    public void shouldEmployeeOnJanuaryRecivSalaryWithNoYearBonus() {
        //given
        Employee employee = EmployeeBuilder
                .anEmployee()
                .correctWorkingEmployee()
                .build();
        Mockito.when(yearEndBonusCalculatorImplMocked.getYearEndBonus(employee, new BigDecimal(SALARY)))
            .thenReturn(new BigDecimal(ZERO_BONUS));
        //when
        BigDecimal payment =  paymentCalculatorImpl.getPaymentAmountForMonth(employee, new BigDecimal(SALARY), JANUARY);
        //then
        assertThat(payment, is(equalTo(new BigDecimal(SALARY))));
    }
    
 

    @Test
    public void shouldEmployeeOnDecemberReciveSalaryWithEndYearBonus() {
        //given
        Employee employee = EmployeeBuilder
                .anEmployee()
                .correctWorkingEmployee()
                .build();
        Mockito.when(yearEndBonusCalculatorImplMocked.getYearEndBonus(employee, new BigDecimal(SALARY)))
            .thenReturn(new BigDecimal(BONUS));
        //when
        BigDecimal payment =  paymentCalculatorImpl.getPaymentAmountForMonth(employee, new BigDecimal(SALARY), DECEMBER);
        //then
        assertThat(payment, is(new BigDecimal(SALARY_WITH_BONUS))); 
    }
    
    
}
