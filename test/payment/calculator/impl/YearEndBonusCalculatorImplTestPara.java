package payment.calculator.impl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import employee.model.Employee;
import employee.model.EmployeeBuilder;
import employee.model.EmploymentStatus;

@RunWith(Parameterized.class)
public class YearEndBonusCalculatorImplTestPara {
    
    

    YearEndBonusCalculatorImpl calculator;
    
    private Employee employee;
    private BigDecimal expectedBonus;
    private final static BigDecimal  SALARY = new BigDecimal(1000);
    private final static BigDecimal  BONUS20 = SALARY.multiply(new BigDecimal(0.2));
    private final static BigDecimal  BONUS30 = SALARY.multiply(new BigDecimal(0.3));
    private final static BigDecimal  BONUS50 = SALARY.multiply(new BigDecimal(0.5));
 
    
    static Employee employee1 =EmployeeBuilder.anEmployee()
            .but().withEmploymentStatus(EmploymentStatus.EMPLOYED)
            .but().withDateOfEmployment(LocalDate.of(2009, 12, 8))
            .build();
    
    static Employee employee2 =EmployeeBuilder.anEmployee()
            .but().withEmploymentStatus(EmploymentStatus.EMPLOYED)
            .but().withDateOfEmployment(LocalDate.of(2012, 12, 8))
            .build();
    
    static Employee employee3 =EmployeeBuilder.anEmployee()
            .but().withEmploymentStatus(EmploymentStatus.EMPLOYED)
            .but().withDateOfEmployment(LocalDate.of(2014, 12, 8))
            .build();
    
    static Employee employee4 =EmployeeBuilder.anEmployee()
            .but().withEmploymentStatus(EmploymentStatus.EMPLOYED)
            .but().withDateOfEmployment(LocalDate.of(2015, 12, 8))
            .build();
    
    
   
    @Before
    public void setUp() throws Exception {
        calculator = new YearEndBonusCalculatorImpl();
    }
    
    public YearEndBonusCalculatorImplTestPara(Employee employee, BigDecimal expectedBonus) { 
        this.employee = employee;
        this.expectedBonus = expectedBonus;
    } 
    @Parameters(name = "{index}::{0} + {1} = {2}")
    public static List<Object[]> employeers(){
        return Arrays.asList(new Object[][] {{employee1,BONUS50},
                                             {employee2,BONUS30},
                                             {employee3,BONUS20},
                                             {employee4,BigDecimal.ZERO},
        });
    }
    
    @Test public void testYearEndBonusCalculatorImpl() { 
        System.out.println("Date of employment : " + employee.getDateOfEmployment());
        assertEquals(expectedBonus, calculator.getYearEndBonus(employee, SALARY)); 
    }   
}
    

    

    

