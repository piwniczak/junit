package employee.service.impl;

import java.math.BigDecimal;
import java.util.Optional;

import contract.service.ContractServiceExt;
import employee.model.Employee;
import employee.service.EmployeeService;
import employee.service.NoEmployeeFoundBusinessException;
import employee.store.EmployeeRepository;
import payment.calculator.PaymentCalculator;

public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeRepository employeeRepository;
    private ContractServiceExt contractServiceExt;
    private PaymentCalculator paymentCalculator;
    
    @Override
    public BigDecimal getPaymentAmountForMonth(String name, String surname, Integer month)
            throws IllegalStateException, NoEmployeeFoundBusinessException {
        Employee foundEmployee = findEmployee(name, surname);
        
        BigDecimal salaryFromContract = contractServiceExt.getSalaryFromContract(foundEmployee.getId(), month);
        
        return paymentCalculator.getPaymentAmountForMonth(foundEmployee, salaryFromContract, month);
        
    }

    private Employee findEmployee(String name, String surname) throws NoEmployeeFoundBusinessException {
        Optional<Employee> foundEmployeeOptional = Optional.empty();
        
        for (Employee currentEmployee : employeeRepository.findAll()) {
            if (currentEmployee.getName().equals(name) 
                    && currentEmployee.getSurname().equals(surname)) {
                foundEmployeeOptional = Optional.of(currentEmployee);
                break;
            }
        }
        
        return foundEmployeeOptional.orElseThrow(() -> new NoEmployeeFoundBusinessException(name, surname));
    }
    
    public void setEmployeeRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public void setContractServiceExt(ContractServiceExt contractServiceExt) {
        this.contractServiceExt = contractServiceExt;
    }

    public void setPaymentCalculator(PaymentCalculator paymentCalculator) {
        this.paymentCalculator = paymentCalculator;
    }
}

       
        
        
        
        
        // TODO: Implement and test it!
       // throw new RuntimeException("Not implemenmted yet!");
    
    

