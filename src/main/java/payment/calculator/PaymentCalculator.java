package payment.calculator;

import java.math.BigDecimal;

import employee.model.Employee;

public interface PaymentCalculator {

    BigDecimal getPaymentAmountForMonth(Employee employee, BigDecimal salary, Integer month);
    
}
