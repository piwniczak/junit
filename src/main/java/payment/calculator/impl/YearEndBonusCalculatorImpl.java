package payment.calculator.impl;

import java.math.BigDecimal;
import java.time.LocalDate;

import employee.model.Employee;
import employee.model.EmploymentStatus;
import payment.calculator.YearEndBonusCalculator;

public class YearEndBonusCalculatorImpl implements YearEndBonusCalculator {
    
    @Override
    public BigDecimal getYearEndBonus(Employee employee, BigDecimal salary) {
        LocalDate bonusDate = LocalDate.of(LocalDate.now().getYear(), 12, 31); 
        int durationInWork = bonusDate.getYear() - employee.getDateOfEmployment().getYear();
        System.out.println(durationInWork);

        BigDecimal bonus=new BigDecimal(0.0);
        if(employee.getEmploymentStatus() == EmploymentStatus.SELF_EMPLOYED || 
                employee.getEmploymentStatus() == EmploymentStatus.RETIRED ||
                employee.getEmploymentStatus() == EmploymentStatus.LEAVER)
        {
           return bonus = new BigDecimal(0.0);
        }
        
        if(employee.getEmploymentStatus() == EmploymentStatus.EMPLOYED  )
                 {
            if(durationInWork > 8) {
              return  bonus = salary.multiply(new BigDecimal(0.5));
            } else if (durationInWork > 5) {
              return  bonus = salary.multiply(new BigDecimal(0.3));          
            } else if (durationInWork > 3) {
              return  bonus = salary.multiply(new BigDecimal(0.2));          
            } 
        }
        return bonus; 
        
        
        // TODO: Implement this method with logic:
        // Employees, that are employed longer than 3 years receives year end
        // bonus -> 20% of salary
        // Employees, that are employed longer than 5 years receives year end
        // bonus -> 30% of salary
        // Employees, that are employed longer than 8 years receives year end
        // bonus -> 50% of salary
        // Self-employed employees receive nothing!
       // throw new RuntimeException("Not implemented yet!");
    }
    
}
