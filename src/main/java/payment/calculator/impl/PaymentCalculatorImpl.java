package payment.calculator.impl;

import java.math.BigDecimal;

import employee.model.Employee;
import payment.calculator.PaymentCalculator;

public class PaymentCalculatorImpl implements PaymentCalculator {
    
    
    private YearEndBonusCalculatorImpl yearEndBonusCalculatorImpl;
    
    @Override
    public BigDecimal getPaymentAmountForMonth(Employee employee, BigDecimal salary, Integer month) {
        if(month < 11) {
            return salary;
        } else {
            return yearEndBonusCalculatorImpl.getYearEndBonus(employee, salary).add(salary);
        }
        
    }

    
    public void setYearEndBonusCalculatorImpl(YearEndBonusCalculatorImpl yearEndBonusCalculatorImpl) {
        this.yearEndBonusCalculatorImpl = yearEndBonusCalculatorImpl;
    }
    
}
